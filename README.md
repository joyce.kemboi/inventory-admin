# inventory
This project is a Vue.js application that provides a invenotory management and simple checkout page for an Emergency Medical Technician (EMT) system.

## Features

- Allows users to enter patient information.
- Displays a list of medical supplies available for selection.
- Allows users to add medical supplies to their cart.
- Provides an order summary with the selected medical supplies.
- Allows users to submit their order.

## Technologies Used

- Vue.js
- Vuetify (for UI components)
- Axios (for making HTTP requests)

## Getting Started

### Prerequisites

- Node.js and npm installed on your machine.

### Installation

1. Clone the repository:

2. Navigate to the project directory:

cd vue-js-project


3. Install dependencies:



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


Open your web browser and visit `http://localhost:8080` to view the application.

## License

This project is licensed under the [MIT License](LICENSE).
