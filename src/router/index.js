import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import HomeView from '../views/HomeView';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      requiresAuth: true,
    },
  },

  {
    path: '/login',
    name: 'login',
    component: () => import('@/modules/auth'),
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('@/modules/auth/register'),
  },
  {
    path: '/access-control/users',
    name: 'users',
    component: () => import('@/modules/accessControl/users'),
    meta: {
      requiresAuth: true,
      title: 'Users',

    },
  },
  {
    path: '/access-control/roles',
    name: 'roles',
    component: () => import('@/modules/accessControl/roles'),
    meta: {
      requiresAuth: true,
      title: 'Roles',
    },
  },
  {
    path: '/inventory',
    name: 'inventory',
    component: () => import('@/modules/inventory'),
    meta: {
      requiresAuth: true,
      title: 'Inventory',
    },
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: () => import('@/modules/checkout'),
    meta: {
      requiresAuth: true,
      title: 'Checkout',
    },
  },
  // {
  //   path: '/access-control/permissions',
  //   name: 'permissions',
  //   component: () => import('@/modules/accessControl/permissions'),
  //   meta: {
  //     requiresAuth: true,
  //     title: 'Permissions',

  //   },
  // },

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});
router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    store.dispatch('initAuth');
    const token = localStorage.getItem('jwtToken');

    if (token) {
      next();
    } else {
      next('/login');
    }
  } else {
    next();
  }
});

export default router;
