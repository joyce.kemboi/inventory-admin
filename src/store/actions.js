/* eslint-disable import/no-extraneous-dependencies */

import { Base64 } from 'js-base64';
import axios from 'axios';
import router from '@/router';

export default {
  async initAuth({ commit, dispatch }) {
    const token = localStorage.getItem('jwtToken');

    if (token) {
      const partsOfToken = token.split('.');
      const middleString = Base64.decode(partsOfToken[1]);
      const userID = JSON.parse(middleString).userId;

      const endpoint = `/users/${userID}`;

      const fullPayload = {
        // app: 'http://159.65.175.58/',
        // app: 'http://localhost:3000/users/1',
        app: process.env.VUE_APP_ROOT_API,
        endpoint,
      };

      const user = await dispatch('retrieveUser', fullPayload);

      commit('setAccessToken', token);
      commit('setSession', user);
    }
  },

  async custom_headers({ state }) {
    const authToken = !state.token ? localStorage.getItem('jwtToken') : state.token;

    const param = {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: authToken,
      },
    };

    return param;
  },

  async axiosPostRequest({ state, commit, dispatch }, payload) {
    const { endpoint } = payload;
    const { app } = payload;
    const user = state.sessionData;

    const headers = await dispatch('custom_headers');
    const url = `${process.env.VUE_APP_ROOT_API}${endpoint}`;
    const values = JSON.stringify(payload.params);

    try {
      const response = await axios.post(`${url}`, payload.params, headers);
      return response;
    } catch (error) {
      return error.response;
    }
  },
  async axiosGetRequest({ dispatch }, payload) {
    const headers = await dispatch('custom_headers');
    const { endpoint } = payload;
    const { params } = payload;
    const { app } = payload;
    const { id } = payload;
    console.log('ddf', payload);
    let url = `${app}${endpoint}`;
    url = typeof id === 'undefined' ? url : `${url}/${id}`;

    const values = {
      params,
      headers: headers.headers,
    };
    // eslint-disable-next-line no-restricted-syntax
    for (const value in values) {
      if (values[value] === null || values[value] === undefined) {
        delete values[value];
      }
    }

    try {
      const response = await axios.get(url, values);
      return response;
    } catch (error) {
      return error.response.message;
    }
  },
  clearCache({ commit }) {
    commit('setAccessToken', null);
    commit('setRefreshToken', null);
    localStorage.clear();
  },
  async retrieveUser({ dispatch }, payload) {
    try {
      const res = await dispatch('axiosGetRequest', payload, { root: true });

      return res.data;
    } catch (error) {
      return error.response;
    }
  },

};
