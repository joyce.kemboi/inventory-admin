export default {

  setSelectedMenu(state, val) {
    state.selectedMenu = val;
  },
  setDrawerStatus(state, val) {
    state.drawerStatus = val;
  },
  setAccessToken(state, val) {
    state.token = val;
  },
  setRefreshToken(state, val) {
    state.refreshToken = val;
  },
  setSession(state, val) {
    state.sessionData = val;
  },
  setLoginErrors(state, val) {
    state.loginErrors = val;
  },
  setTokenExpiryStatus(state, val) {
    state.tokenExpired = val;
  },
  setSessionRefreshed(state, val) {
    state.sessionRefreshed = val;
  },
  updateSession(state, data) {
    state.session_data = data;
  },
};
