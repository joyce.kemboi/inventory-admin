import Vue from 'vue';
import Vuex from 'vuex';
import actions from '@/store/actions';
import getters from '@/store/getters';
import mutations from '@/store/mutations';

Vue.use(Vuex);

const state = {
  selectedMenu: null,
  drawerStatus: true,
  token: null,
  authenticationStatus: null,
  sessionData: null,
  // refreshToken: null,
  // loginErrors: null,
  // tokenExpired: false,
  // sessionRefreshed: false,

};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};
