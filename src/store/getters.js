export default {
  getSelectedMenu: (state) => state.selectedMenu,
  getDrawerStatus: (state) => state.drawerStatus,
  isAuthenticated: (state) => state.token !== null,
  getSession: (state) => state.sessionData,
  getloginErrors: (state) => state.loginErrors,
  getTokenExpiryStatus: (state) => state.tokenExpired,
  getSessionRefreshed: (state) => state.sessionRefreshed,

};
